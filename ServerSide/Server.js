var express = require('express')
var app = express()
var sql = require('sqlite3').verbose();
var dbFile = __dirname + '/' + 'MusicRoom.db';
var db = new sql.Database(dbFile);

db.serialize();

app.use(express.static(__dirname + '/webside'))
app.get('/', function(req, res){
    res.sendFile(__dirname + "/webside/views/index.html");
})

// Get room's music-list 
app.get('/Music/:room', function(req, res){
    db.all("SELECT * FROM "  + req.params.room, function(err, rows){
        if (err === null) {
            res.send(rows);
        } else {
            res.send("Oops that was unexpected was it...");
        }
    });
});

// Starting the server
var port = 3000;
app.listen(port, function () {
  console.log(new Date() + '\nMusicRoom server started: '+ port );
});

